module TiposCompuestos where

data Polinomio = Pol Grado Coefs
type Grado = Int
type Coefs = [Int]

grado (Pol g cs) = g
coefs (Pol g cs) = cs

data Vector = Vec VecX VecY
type VecX = Int
type VecY = Int

vecx (Vec x y) = x
vecy (Vec x y) = y

data Matriz = Mat Fil Col
type Fil = Int
type Col = Int

fil (Mat f c) = f
col (Mat f c) = c

-- Problemas
eval :: Polinomio -> Int -> Int
eval p x
	| length (coefs p) == (grado p)+1 = evalAux p x (grado p)
	| otherwise = error "polinomio mal construido (faltan coeficientes)"

evalAux p x g
	| g==0 = (coefs p)!!0
	| otherwise = ((coefs p)!!g)*x^g + (evalAux p x (g-1))

