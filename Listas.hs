module Listas (sumaLista, mismos, sacarTodos, sacarRep, contar, palabras, listaMasLarga, aplanar, aplanar_, aplanarDX, nat2b) where

import Basico

--Practica 7
--1)
ultimo [] = error "conjunto vacio"
ultimo [x] = x
ultimo (x:xs) = ultimo xs

principio [] = error "conjunto vacio"
principio [x] = []
principio (x:xs) = x:(principio xs)

todosIguales [] = error "conjunto vacio"
todosIguales [x] = True
todosIguales (x1:x2:xs) = x1 == x2 && todosIguales (x2:xs)

todosDistintos [] = error "conjunto vacio"
todosDistintos [x] = True
todosDistintos (x:xs)
	| x `elem` xs = False
	| otherwise = todosDistintos xs

mismos [] [] = True
mismos [] ys = False
mismos xs [] = False
mismos (x:xs) (y:ys) = todoxeny (x:xs) (y:ys) && todoyenx (x:xs) (y:ys)

todoxeny [] ys = True
todoxeny (x:xs) ys = x `elem` ys && todoxeny xs ys

todoyenx xs [] = True
todoyenx xs (y:ys) = y `elem` xs && todoyenx xs ys

--2)

sacarTodos [] ys = ys
sacarTodos xs [] = []
sacarTodos xs (y:ys)
	| y `elem` xs = sacarTodos xs ys
	| otherwise = y:(sacarTodos xs ys)


sacarRep aes [] = []
sacarRep aes [x] = [x]
sacarRep aes (x1:x2:xs)
	| x1 `elem` aes && x2 `elem` aes = sacarRep aes (x2:xs)
	| otherwise = x1:(sacarRep aes (x2:xs))


contarPalabras [] = 0 --cuenta los espacios
contarPalabras x
	| head texto == ' ' && last texto == ' ' = (contar ' ' texto) - 1
	| head texto == ' ' || last texto == ' ' = contar ' ' texto
	| otherwise = (contar ' ' texto) + 1
	where texto = sacarRep " " x


contar a [] = 0
contar a (x:xs)
	| x==a = 1 + contar a xs
	| otherwise = contar a xs


palabras [] = error "no hay palabras"
palabras xs --filtros previos para acomodar la entrada así empieza con un espacio
	| (head texto)==' ' && (last texto)==' ' = palabrasPosta (init texto)
	| (head texto)==' ' = palabrasPosta texto
	| (last texto)==' ' = palabrasPosta (init (' ':texto))
	| otherwise = palabrasPosta (' ':texto)
	where texto = sacarRep " " xs --previamente le saco todos los espacios repetidos
palabrasPosta [] = [] --en cada espacio llamo a primerPalabra y la agrego
palabrasPosta (x:xs)
	| x==' ' = (primerPalabra xs):(palabrasPosta xs)
	| otherwise = palabrasPosta xs	
primerPalabra [] = [] --obtengo la primer palabra del resto del string
primerPalabra (x:xs)
	| x==' ' = []
	| otherwise = x:(primerPalabra xs)


palabraMasLarga xs = listaMasLarga (palabras xs)


listaMasLarga [] = []
listaMasLarga (xs:xss)
	| length xs >= length (listaMasLarga xss) = xs
	| otherwise = listaMasLarga xss


aplanar [] = []
aplanar (xs:xss) = xs++(aplanar xss)


aplanar_ xss = aplanarDX 1 " " xss

--Extra: aplana con n espacios "a" (a debe ser del mismo tipo que la lista)
aplanarDX n a [] = []
aplanarDX n a (xs:xss) = xs++(concatenar n (espacio a (xss)))++(aplanarDX n a xss)

espacio a [] = []
espacio a xss = a

concatenar n xs
	| n==0 = []
	| otherwise = xs++(concatenar (n-1) xs)

--Extra: pasa un natural en base 10 a base b<=37; a partir de b=37 aparecen "gaps" en la salida (intervalos de números que no se pueden mostrar por falta de símbolos -> error "!! out of range")
nat2b n b = if b < 2 then error "base imposible"
			else letrear (reverse (nat2b' n b))
nat2b' n b = if n < b then [n]
			else (mod n b):(nat2b' (div n b) b)

letrear [] = []
letrear (n:ns) = ((['0'..'9']++['A'..'Z'])!!n):(letrear ns)

--Extra: pasa un natural en base b a base 10; a partir de b=36 aparecen "gaps" en la entrada (intervalos de números que no se pueden ingresar por falta de símbolos)
b2nat [] b = error "no hay numero"
b2nat ns b = if b < 2 then error "base imposible"
			else b2nat' (numerar ns) b
b2nat' [] b = 0
b2nat' (n:ns) b = if n >= b then error "numero imposible"
				else n*b^(length ns) + b2nat' ns b

numerar [] = []
numerar (n:ns) = (pos n (['0'..'9']++['A'..'Z'])):(numerar ns)

pos a [] = error "no está"
pos a aes = if a==last(aes) then (length aes)-1 else pos a (init(aes))

{-pending:
--3)
nreduc

--4)
subQueSumaMas

--5)
ordenA
ordenD
listarElemRep

--6)
reverso
capicua
sumaAcumulada
descomponerEnPrimos

-}





