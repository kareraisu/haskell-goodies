module Basico (esPrimo, primosEntre, superPot, long, cifras, sumaCifras, sumaTotal, sumaLista) where

--Práctica 6
--4)
esPrimo :: Int -> Bool
esPrimo n = esPrimoAux n 2

esPrimoAux :: Int -> Int -> Bool
esPrimoAux n i
	| n == 1 = False
	| n == i = True
	| n `mod` i == 0 = False
	| otherwise = esPrimoAux n (i+1)

--Extra
primosEntre :: Int -> Int -> [Int]
primosEntre n m
	| n == m+1 = []
	| esPrimo n = n:(primosEntre (n+1) m)
	| otherwise = primosEntre (n+1) m

primosEntre' n m = (res, length res) where res = filter esPrimo [n..m]

--5) Esp: f (n: Z) : Z = if n==0 then 0 else n^(n^(n-1))
superPot n --ta hablando del fasooo :P
	| n == 0 = 0
	| otherwise = n `pot` (n `pot` (n-1))

pot n m
	| m == 0 = 1
	| otherwise = n*(pot n (m-1))

--6)
divx3 :: Integer -> Bool
divx3 n = (sumaCifras n) `mod` 3 == 0

sumaCifras n = sumaLista (cifras n)


cifras n = cifras' n (long n) 0
cifras' n p i
	| p == 0 = []
	| n == 0 = 0:(cifras' n (p-1) i)
	| (n - i*(10^(p-1))) `div` 10^(p-1) == 0 = i:(cifras' (n - i*(10^(p-1))) (p-1) 0)
	| otherwise = cifras' n p (i+1)

long n = long' n 1
long' n i
	| n `div` 10 == 0 = i
	| otherwise = long' (n `div` 10) (i+1)


sumaLista [] = 0
sumaLista (x:xs) = x + sumaLista xs

--Extra
sumaTotal n = if (long n) == 1 then n
			else sumaTotal (sumaCifras n)


--7) Pending
--8) Pending



